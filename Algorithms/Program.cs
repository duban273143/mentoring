using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;

            Console.WriteLine("How many sequence values?");
            n = Convert.ToInt32(Console.ReadLine());

            Fibonacci(n);
        }

        //iteracyjnie
        public static void Fibonacci(int n)
        {
            long a = 0;
            long b = 1;

            for (int i = 0; i < n; i++)
            {
                b += a;
                a = b - a;
            }
        }
        //rekurencyjnie
        public static int Fibonacci2(int n)
        {
            if (n < 3)
                return 1;

            return Fibonacci2(n - 2) + Fibonacci2(n - 1);
        }

        //przeszukiwanie binarne
        public static int BinarySearch(int[] list, int l, int r, int x)
        {
            if (r >= 1)
            {
                int mid = 1 + (r - 1) / 2;

                if (list[mid] == x)
                    return mid;

                if (list[mid] > x)
                    return BinarySearch(list, l, mid-1, x);

                return BinarySearch(list, mid + 1, r, x);
            }

            return -1;
        }
        //nwd
        public static int NWD(int a, int b)
        {
            while(a!=b)
                if (a > b)
                {
                    a -= b;
                }
                else
                {
                    b -= a;
                }

            return a;
        }
        //sito Erastotenesa
        public static void Erastotenes(int n)
        {
            
        }
    }
}
